package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Sergey Milovsky on 22.02.2017.
 */
public class SubsequenceImpl implements Subsequence {


    @Override
    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        Iterator iteratorX = x.iterator();
        Iterator iteratorY = y.iterator();

foo:    while (iteratorX.hasNext()) {
            Object objX = iteratorX.next();

            while (iteratorY.hasNext()) {
                Object objY = iteratorY.next();
                if (objX.equals(objY)) {
                    continue foo;
                }
            }
            return !iteratorX.hasNext();
        }
        return true;
    }
}

