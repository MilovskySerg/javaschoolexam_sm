package com.tsystems.javaschool.tasks.duplicates;

import com.sun.javafx.collections.MappingChange;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Sergey Milovsky on 22.02.2017.
 */
public class DuplicateFinderImpl implements DuplicateFinder {


    @Override
    public boolean process(File sourceFile, File targetFile) {

       if (sourceFile == null || targetFile == null) {
           throw new IllegalArgumentException();
       }

        List<String> list;
        try {
            list = Files.readAllLines(Paths.get(sourceFile.toURI()));
        } catch (NoSuchFileException | FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        Map<String, Integer> map = new TreeMap<String, Integer>();          //create TreeMap for add and sort

        for (String s : list) {
            map.put(s, Collections.frequency(list, s));
        }

        FileWriter out = null;
        try {
            out = new FileWriter(targetFile, true);                    //write to targetFile
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                out.write(entry.getKey() + "[" + entry.getValue() + "]" + "\n");
            }
            System.out.println("output file: " + targetFile.getAbsolutePath());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally{
            if (out != null){
                try {
                    out.close();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }

        return true;
    }
}
