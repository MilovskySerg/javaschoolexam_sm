package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.Locale;

/**
 * Created by Sergey Milovsky on 21.02.2017.
 */
public class CalculatorImpl implements Calculator {

    @Override
    public String evaluate(String statement) {
        if (!isExprValid(statement, 0, 0)) {
            return null;
        } else {

            LinkedList<Double> operands = new LinkedList<>();
            LinkedList<Character> operators = new LinkedList<>();

            try {
                for (int i = 0; i < statement.length(); i++) {
                    char c = statement.charAt(i);

                    if (c == '(') {
                        operators.add(c);
                    } else if (c == ')') {
                        while (operators.getLast() != '(') {
                            calculate(operands, operators.removeLast());
                        }
                        operators.removeLast();
                    } else if (isOperator(c)) {
                        while (!operators.isEmpty() && priority(operators.getLast()) >= priority(c)) {
                            calculate(operands, operators.removeLast());
                        }
                        operators.add(c);
                    } else {
                        String operand = "";

                        while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                            operand += statement.charAt(i++);
                        }
                        --i;
                        operands.add(Double.parseDouble(operand));
                    }
                }
                while (!operators.isEmpty()) {
                    calculate(operands, operators.removeLast());
                }
            } catch (ZeroDivisionException | NumberFormatException | UnsupportedOperationException e) {
                return null;
            }
            double result = operands.get(0);
            if (result % 1 == 0)
                return format(result);
            else
                return format(round(result, 4));
        }
    }

    private boolean isOperator(char c) {                        //is char operator?
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private int priority(char operator) {
        if (operator == '*' || operator == '/') {
            return 1;
        } else if (operator == '+' || operator == '-') {
            return 0;
        } else {
            return -1;
        }

    }

    private void calculate(LinkedList<Double> operands, char operator) { //считать
        double operandA = operands.removeLast();
        double operandB = operands.removeLast();

        switch (operator) {
            case '+':
                operands.add(operandB + operandA);
                break;
            case '-':
                operands.add(operandB - operandA);
                break;
            case '*':
                operands.add(operandB * operandA);
                break;
            case '/':
                if (operandA != 0)
                    operands.add(operandB / operandA);
                else throw new ZeroDivisionException();

                break;
            default:
                throw new UnsupportedOperationException("invalid operator");
        }
    }

    private boolean isExprValid(String expr, int beginIndex, int endIndex) {
        if (expr == null || expr.isEmpty())
            return false;
        else {
            if (beginIndex < expr.length()) {
                if (expr.charAt(beginIndex) == '(' && endIndex > -1) {
                    endIndex++;
                    beginIndex++;
                    isExprValid(expr, beginIndex, endIndex);
                } else if (expr.charAt(beginIndex) == ')' && endIndex > 0) {
                    endIndex--;
                    beginIndex++;
                    isExprValid(expr, beginIndex, endIndex);
                } else {
                    return true;
                }
            }
            return false;
        }
    }

    private class ZeroDivisionException extends ArithmeticException {}

    private String format(double d) {               //format double result
        if (d == (long) d)
            return String.format(Locale.US, "%d", (long) d);
        else
            return String.format(Locale.US, "%s", d);
    }

    private double round(double value, int places) {      //round double result
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
